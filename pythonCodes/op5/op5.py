import argparse



header = ("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
          "            <svg viewBox=\"0 0 500 50\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n"
          "        ")
end_file= '</svg>'
first = '<rect x="'
x = 0
second = '" y="0" width="'
width = 0
third = '" height="50" fill="'
fill = 'red'
fourth = '" />'

prev_width = 0
start_time = 0
prev_status = 'blue'
o = open('output', "w")
with open('testdata_small.txt') as fh:
    o.write(header)
    for line in fh:
        width = line.split(' ')[0]
        status = line.split(' ')[1].rstrip()
        if status == 'True':
            status = 'green'
        else:
            status = 'red'
        if status != prev_status:
            # ignore if a new event is recorded, but the status has not changed
            new_line = first + str(prev_width) + second + str(width) + third + status + fourth + '\n'
            o.write(new_line)
            prev_width = int(width) + int(prev_width)
            prev_status = status


o.write(end_file)
o.close()


