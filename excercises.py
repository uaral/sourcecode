from functools import cmp_to_key
import pytest

def calculatePercentage(current, total):
    print((current / total) * 100)

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '', flush=True)
    # Print New Line on Complete
    if iteration == total: 
        print()


"""
Advent of code 2018, day 5, part 2
If we were to remove one character e.g. all a/A or all b/Bs
and so on ... and then reduce the string, what would be the
shortest possible length of a reduced string.

Solution: uses same functionalities used in part1
"""
def findShortestReducedString(string):
    print("findShortestReducedString in progress:")
    ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
    shortest_length = 1000000
    for index,c in enumerate(ALPHABET):
        printProgressBar(index + 1, 26, prefix = 'Parent:', suffix = 'Complete', length = 50)
        tmp = reduceString(string, c)
        if tmp < shortest_length:
            shortest_length = tmp
            #print("shortest Updated with " + str(shortest_length))
    return shortest_length

def findShortestReducedStringTest():
    f = open("day5_p2", "r")
    tmp = f.readline().rstrip("\n")
    string = tmp.rstrip("\n")
    assert(6946 == findShortestReducedString(string))
    print("PASSED findShortestReducedStringTest")

"""
Advent of code 2018, day 5, part 1
e.g. dabAcCaCBAcCcaDA is reduced to dabCBAcaDA
return the length of the reduced string.

exclude = character to remove from string, only used for part 2
"""
def reduceString(string = "dabAcCaCBAcCcaDA", exclude=False):
    def canReduce(a, b):
        # assume a, b are alphabet characters
        if abs(ord(a) - ord(b)) == abs(ord('a') - ord('A')):
            return True
        return False

    string_len = len(string)
    i = 1

    # Special handling of first char, since we start with i = 1 and not i = 0
    def checkFirstCharacter(string, exclude, string_len):
        if (exclude and string[0].lower() == exclude.lower()):
            string = string[1:]
            string_len = string_len - 1
            checkFirstCharacter(string, exclude, string_len)
    checkFirstCharacter(string, exclude, string_len)

    while (i < string_len):
        printProgressBar(i + 1, string_len, prefix = 'Progress:', suffix = 'Complete', length = 50)
        #print(string_len)
        #print(i)
        if (exclude and string[i].lower() == exclude):
            pref = string[:i]
            post = string[i+1:]
            string = pref + post
            i = max(i - 1, 1) # See comment below, same safeguard
            string_len = string_len - 1
            continue
        if (canReduce(string[i - 1], string[i])):
            pref = string[:i - 1]
            post = string[i + 1:] # this is fine, will not throw exception
            string = pref + post
            i = max(i - 2, 1) # very corky here, if i = 1 and i=0 and i=1
                              # is deduceable, xX, i becomes negative
                              # if i = -1 is same as i = string_len
                              # if that matches exclude char, we will not
                              # have an exit scenario and loop forever.
            string_len = string_len - 2
        else:
            i += 1

    return string_len

def reduceStringTest():
    f = open("day5_p1", "r")
    tmp = f.readline().rstrip("\n")
    string = tmp.rstrip("\n")
    assert(10762 == reduceString(string))
    print("PASSED reduceStringTest")



# """
# Advent of code 2018, day 3, part 1 WIP
# """
# def getOverlappingArea(claims):
#     class Rectangle:
#         # __init__ is the constructor of the class
#         def __init__(self, claim):

#             self.llx = int(claim.split("@")[1].split(",")[0])  # llx refers to Lower Left X
#             self.lly = int(claim.split("@")[1].split(",")[1].split(":")[0])
#             self.lrx = 
#             self.lry = 
#             self.ulx = 
#             self.uly = 
#             self.urx =  # urx refers to Upper Right x
#             self.ury = 
#             self.id = claim.split("@")[0].split("#")[1]
#             self.__privatevariable = "this is private"

#         def __repr__(self):
#             return "name is " + self.name

#         def __str__(self):
#             return "0"

#         angles = 0

#         def dec(self):
#             print("shape class")

#         def get_private(self):
#             print(self.__privatevariable)

"""
Advent of code 2018, day 2, part 2
"""
def findCommonLettersOfBoxIDs(boxIDs):
    # if the difference is less than one char, then its a match
    def isMatch(a,b):
        diff = 0
        index = -1
        for i in range(0, len(a)):
            if a[i] == b[i]:
                continue
            else:
                diff += 1
                index = i
                if diff > 1:
                    return False
        return(a[:index] + a[index +1:]) # the actual answer

    for x in range(0, len(boxIDs)):
        for y in range(1, len(boxIDs)):
            if x != y and isMatch(boxIDs[x], boxIDs[y]):
                return isMatch(boxIDs[x], boxIDs[y])
                # print(x)
                # print(y)
                # print(boxIDs[x])
                # print(boxIDs[y])

def findCommonLettersOfBoxIDsTest():
    assert (("mphcuasvrnjzzkbgdtqeoylva\n") ==
        findCommonLettersOfBoxIDs(makeListFromFile("day2_part2_input")))
    print("PASSED findCommonLettersOfBoxIDsTest AoC D2-2")

"""
Advent of code 2018, day 2, part 1
get checksum of list of box IDs
for a given list of strings, count the number of strings that have exactly 2 or
exactly 3 of the same char. Multiply the number of each case and return.
abcdef contains no letters that appear exactly two or three times.
bababc contains two a and three b, so it counts for both.
abbcde contains two b, but no letter appears exactly three times.
abcccd contains three c, but no letter appears exactly two times.
aabcdd contains two a and two d, but it only counts once.
abcdee contains two e.
ababab contains three a and three b, but it only counts once.
"""
def getChecksum(boxIDs):
    twos = 0
    threes = 0

    def howmany(string):
        myDict = {}
        numberOfExactlyTwos = 0
        numberOfExactlyThrees = 0
        for c in string:
            myDict[c] = myDict.get(c, 0) + 1

        for e in myDict:
            if myDict[e] == 2:
                numberOfExactlyTwos = 1
            elif myDict[e] == 3:
                numberOfExactlyThrees = 1
        return (numberOfExactlyTwos, numberOfExactlyThrees)

    for e in boxIDs:
        (two, three) = howmany(e)
        twos = twos + two
        threes = threes + three

    return twos * threes

def getChecksumTest():
    inputExample = ["abcdef","bababc","abbcde","abcccd","aabcdd","abcdee","ababab"]
    assert(12 == getChecksum(inputExample))
    assert(7533 == getChecksum(makeListFromFile("day2_part1_input")))
    print("PASSED getChecksumTest AoC D2-1")

"""
Advent of code 2018, day 1 part 2
find the first frequency that repeats itself.
"""
def findFirstDuplicateFrequency(listOfChanges):
    seenFrequencies = {}
    newFrequency = 0
    for i in range(0, 1000):
        for e in listOfChanges:
            newFrequency = newFrequency + e
            seenFrequencies[newFrequency] = seenFrequencies.get(newFrequency, 0) + 1
            if seenFrequencies[newFrequency] == 2:
                return(newFrequency)
    return ("no duplicate frequencies")

def findFirstDuplicateFrequencyTest():
    f = open("day1_part2_input", "r")
    puzzleInput = []
    for line in f.readlines():
        puzzleInput.append(int(line))
    assert(76787 == findFirstDuplicateFrequency(puzzleInput))
    print("PASSED findFirstDuplicateFrequencyTest AoC D1-2")



"""
Advent of code 2018, day 1 part1
"""
def getNewFrequency(listOfChanges):

    newFrequency = 0
    for i,e in enumerate(listOfChanges):
        newFrequency = newFrequency + e
        #calculatePercentage(i,len(listOfChanges))

    return(newFrequency)

def getNewFrequencyTest():
    f = open("day1_input", "r")
    puzzleInput = []
    for line in f.readlines():
        puzzleInput.append(int(line))
    assert(531 == getNewFrequency(puzzleInput))
    print("PASSED getNewFequencyTest AoC D1-1")



"""
Given an array of numbers, arrange them in a way that yields the largest value. For example, if the given numbers are
{54, 546, 548, 60}, the arrangement 6054854654 gives the largest value. And if the given numbers are
{1, 34, 3, 98, 9, 76, 45, 4}, then the arrangement 998764543431 gives the largest value.
https://www.geeksforgeeks.org/given-an-array-of-numbers-arrange-the-numbers-to-form-the-biggest-number/
"""
def arrangeGivenNumbersToFormBiggestNumber(S):

    def compareDigit(a, b):
        return (int(str(a) + str(b)) - int(str(b) + str(a)))

    #find the number with biggest first digit, if equal, find the biggest second digit ...
    res = sorted(S, reverse=True, key=cmp_to_key(compareDigit)) # in python 3 cmp_to_key is needed to provide compare function
    return( int(''.join("{0}".format(n) for n in res)))



def arrangeGivenNumbersToFormBiggestNumberTest():
    assert(998764543431 == arrangeGivenNumbersToFormBiggestNumber([1, 34, 3, 98, 9, 76, 45, 4]))
    assert(6054854654 == arrangeGivenNumbersToFormBiggestNumber({54, 546, 548, 60}))
    print("PASSED arrangeGivenNumbersToFormBiggestNumberTest")



"""
An sms consists of words seperated by one, and only one space and limited by the number of characters it can send.
Given a string of words S, and the maximum number of characters in each sms, find out how many sms messages are needed
for S to be transmitted such that no words would be truncated. For example, given S="sms messages are really short"
and K=12 should return 3 whilst "thisisaverylongwordthatwillnotfit" and K=12 should return False.
"""
def countNumberOfSms(S, K):
    word_list = S.split(" ")
    cur_len = 0
    sms_count = 0
    space = 1
    last = len(word_list) - 1
    for index, word in enumerate(word_list):
        if cur_len == 0: # beginning of new sms
            sms_count = sms_count + 1
        if len(word) > K:
            return False
        elif (cur_len + len(word)) > K:
            sms_count = sms_count + 1
            cur_len = len(word)
            continue
        elif (cur_len + len(word)) == K:
            cur_len = 0
            continue
        else:
            cur_len = cur_len + len(word) + 1
            continue

    return sms_count

def countNumberOfSmsTest():
    assert(1 == countNumberOfSms("s", 12))
    assert(1 == countNumberOfSms("s s", 12))
    assert(2 == countNumberOfSms("1 2 3 4 5 6 7 8 9 0 1 1", 12))
    assert(False == countNumberOfSms("1 2 3 4 5 6 7 8 9 0 1 1 asdfa", 4))
    assert(False == countNumberOfSms("sms message1 are reall2 short an3 aasdf4 123456789sdf015", 12))
    assert(False == countNumberOfSms("this is a test", 1))
    assert(False == countNumberOfSms("sfsdfs sfdsfsfs", 2))

    assert(1 == countNumberOfSms("sms message1", 12))
    assert(3 == countNumberOfSms("sms message1 are reall2 shor3", 12))
    assert(4 == countNumberOfSms("sms message1 are reall2 shor3 123456789014", 12))
    assert(5 == countNumberOfSms("sms message1 are reall2 short an3 aasdf4 123456789015", 12))
    assert(3 == countNumberOfSms("this is a test", 4))
    assert(3 == countNumberOfSms("the the the", 3))
    assert(1 == countNumberOfSms("this", 4))
    assert(5 == countNumberOfSms("this is so much good", 4))
    assert(5 == countNumberOfSms("th sd sds sdsd a", 4))
    print("PASSED countNumberOfSmsTest")

"""
Goldman Sachs question No.1:
Pangram Detector

The sentence "The quick brown fox jumps over the lazy dog" contains
every single letter in the alphabet. Such sentences are called pangrams.

Write a function findMissingLetters, which takes a string `sentence`,
and returns all the letters it is missing (which prevent it from
being a pangram). You should ignore the case of the letters in sentence,
and your return should be all lower case letters, in alphabetical order.
You should also ignore all non US-ASCII characters.
"""
ALPHABET = 'abcdefghijklmnopqrstuvwxyz'

# TODO: complete this function stub
def FindMissingLetters(sentence):
#    print(sentence)
    my_dict = {}
    lowercased = sentence.lower()
    for e in lowercased:
        if e in ALPHABET:
            my_dict[e] = my_dict.get(e, 0) + 1
    myString = ''
#    print(my_dict)
    for c in ALPHABET:
        if c not in my_dict:
            myString = myString + str(c)

#    print(myString)
    return myString
# Test Cases

def FindMissingLettersTest():
    success = (
        '' == FindMissingLetters("The quick brown fox jumps over the lazy dog") and
        'bfgjkvz' == FindMissingLetters("The slow purple oryx meanders past the quiescent canine") and
        'cdfjklmopqruvxyz' == FindMissingLetters("We hates Bagginses!") and
        'abcdefghijklmnopqrstuvwxyz' == FindMissingLetters("") and
        'abcdefghijklmnopqrstuvwxyz' == FindMissingLetters("/////////////") # this test was added by me,
                                                                            # the rest, already were already there

    )
    if success:
        print("PASSED FindMissingLettersTestAll")
    else:
        print("At least one test failed.")

"""
Goldman Sachs question No.2:
Instructions to candidate.
 1) Your task is ultimately to implement a function that takes in an array of non-negative numbers and an integer.
    You want to return the *LENGTH* of the shortest subarray whose sum is at least the integer,
    and -1 if no such sum exists.
 2) Run this code in the REPL to observe its behaviour. The
    execution entry point is main().
 3) Consider adding some additional tests in doTestsPass().
 4) Implement subArrayExceedsSum() correctly.
 5) If time permits, some possible follow-ups.

 Safa: https://www.geeksforgeeks.org/minimum-length-subarray-sum-greater-given-value/
"""
def subArrayExceedsSum(arr, target):
    arr_size = len(arr)
    start = 0
    end = 0
    total = 0
    min_len = -1
    while(end < arr_size):
        while (total < target and end < arr_size):
            total = total + arr[end]
            end += 1

        while (total >= target and start < end):
            if (total >= target):
                min_len = end - start
            total = total - arr[start]
            start += 1

    return min_len

def subArrayExceedsSumTest():
    """ Returns 1 if all tests pass. Otherwise returns 0. """
    testArrays    = [[[1,2,3,4], 6], [[1,2,3,4], 12], [[0,0,0,0], 1], [[3,1,1,1], 2], [[3,1,2,1], 2], [[7,1,2,7], 7]]
    testAnswers   = [2, -1, -1, 2, 1, 1, 1]
    success = True
    for i in range( len( testArrays ) ):
        if not ( subArrayExceedsSum( testArrays[i][0], testArrays[i][1] ) == testAnswers[i] ):
            success = False
            break

    if success:
        print("PASSED subArrayExceedsSumTest")
    else:
        print("\033[91mFAILED subArrayExceedsSumTest \033[0m")

"""
Todo: currently not working for all corner cases
"""
def smsNavidSolution(sms, max_len):
    sms_count = 0
    cur_len = 0
    leftover = False
    for word in sms.split(' '):
        if len(word) > max_len:
            return -1
        if len(word) == max_len:
            if leftover:
                sms_count += 2
                leftover = False
            else:
                sms_count += 1
        if len(word) < max_len:
            if cur_len == 0:
                cur_len = len(word)
                continue
            elif cur_len  + len(' ') + len(word) < max_len:
                cur_len += len(word)
            elif cur_len  + len(' ') + len(word) > max_len:
                sms_count += 1
                cur_len = len(word)
                leftover = True
            elif cur_len  + len(' ') + len(word) == max_len:
                sms_count += 1
                cur_len = 0

    return sms_count

def test():
    print("Running tests:")
    subArrayExceedsSumTest()
    arrangeGivenNumbersToFormBiggestNumberTest()
    countNumberOfSmsTest()
    getNewFrequencyTest()
    findFirstDuplicateFrequencyTest()
    getChecksumTest()
    findCommonLettersOfBoxIDsTest()
    FindMissingLettersTest()
    reduceStringTest()
    findShortestReducedStringTest()

def main():
    test()
    return
    print("this s test")
    print(10 * "*")
    print(None)
    obj = Shape("square")
    obj.dec()
    obj.get_private()
    print(True or False)
    return
    print(obj.name)
    print("obj printed returns:")
    print([obj])
    print(obj)

"""
Takes a filename consisting of one element in each line
and returns a list of those elements in the file.
"""
def makeListFromFile(filename):
    elements = []
    f = open(filename, "r")
    for l in f.readlines():
        elements.append(l)
    return elements

if __name__ == "__main__":
    main()



"""
ALL NONE PERMANENT AND exploratory code should be placed below here
"""

"""
perhaps volvo question?
"""
def solution(A):
    small_index = A[0]
    big_index = [1]
    for i, e in enumerate(A):
        best_pos = abs(big_index - small_index) / 2
        if e > big_index:
            big_index = e
        elif e < small_index:
            small_index = e
        elif e < (big_index - small_index) / 2:
            small_index = e
        elif e > (big_index - small_index) / 2:
            big_index = e


"""
This is a general example of classes in python.
"""

class Shape:
    # __init__ is the constructor of the class
    def __init__(self, name):
        self.name = name
        self.__privatevariable = "this is private"

    def __repr__(self):
        return "name is " + self.name

    def __str__(self):
        return "0"

    angles = 0

    def dec(self):
        print("shape class")

    def get_private(self):
        print(self.__privatevariable)
