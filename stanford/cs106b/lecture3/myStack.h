#ifndef _MyStack_H
#define _MyStack_H
#include "myVector.h"

template <typename elemType>
class myStack
{
public:
    myStack();
    ~myStack();
    bool isEmpty();
    void push(elemType);
    elemType pop();

private:

    struct cellT {
        elemType val;
        cellT * next;
    };

    cellT* head;

};

template <typename elemType>
myStack<elemType>::~myStack()
{
    //delete entire stack
}


template <typename elemType>
myStack<elemType>::myStack()
{
    head = NULL;
}

template <typename elemType>
bool myStack<elemType>::isEmpty()
{
    if(head != NULL)
        return false;
     else
        return true;
}
template <typename elemType>
void myStack<elemType>::push(elemType e)
{
    cellT* newCell = new cellT;
    newCell->val = e;
    newCell->next = head;
    head = newCell;
}

template <typename elemType>
elemType myStack<elemType>::pop()
{
    if (isEmpty())
        return NULL;
    cellT* old = head;
    head = old->next;
    return old->val;
}

#endif
