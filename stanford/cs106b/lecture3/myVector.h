#ifndef _MyVector_H
#define _MyVector_H

#include <iostream>
#include <string>

template <typename elemType>
class myVector
{
public:
    myVector();
    ~myVector();

    int size();
    void add(elemType s);
    elemType getAt(int index);
    void insertAt(int index, elemType elem);
    void deleteLast();

private:
    elemType *arr; //single pointer that points to string
    int numUsed, numAllocated;
    void doubleCapacity();

};


/*
 * Here onwards is what should be in the cpp file, except that
 * only and only in templates, the definitions reside in the header
 * file as well.
 */

template <typename elemType>
void myVector<elemType>::deleteLast()
{
    numUsed--;
}

template <typename elemType>
myVector<elemType>::myVector()
{
    arr = new elemType[2];
    numAllocated = 2;
    numUsed = 0;
}

template <typename elemType>
myVector<elemType>::~myVector(){
    delete[] arr;
}

template <typename elemType>
int myVector<elemType>::size()
{
    return numUsed;
}

template <typename elemType>
void myVector<elemType>::add(elemType s)
{
    if (numUsed == numAllocated)
        doubleCapacity();
    arr[numUsed++] = s;
}

template <typename elemType>
elemType myVector<elemType>::getAt(int index)
{
    if (index < 0 || index > size())
        std::cout << "out of bounds";
    return arr[index];
}

template <typename elemType>
void myVector<elemType>::doubleCapacity()
{
    std::cout << "doubling capacity" << std::endl;
    elemType *newArr = new elemType[2 * numAllocated];
    for(int i = 0; i < numAllocated ; i++ )
    {
        newArr[i] = arr[i];
    }
    delete[] arr; /* everytime the old new arr[] gets deleted, so the at any point there is one arr
                    in heap, which is deleted in deconstructor.
                    also, delete[] knows that it is deleting a what arr is pointing to in heap,
                    arr itself is on stack.
                    "delete" however deletes what it takes, which itself is sitting on heap, not what
                    it points to.
                    */
    numAllocated *= 2;
    arr = newArr;

}

template <typename elemType>
void myVector<elemType>::insertAt(int index, elemType elem)
{
    if (numUsed == numAllocated)
        doubleCapacity();
    for(int i =  size() - 1; index <= i; i-- )
    {
        arr[i + 1] = arr[i];
    }
    arr[index] = elem;
    numUsed++;

}
//template <typename elemType>
//void myVector<elemType>::insertAt(int index, elemType elem)
//{
//    elemType *newArr = new elemType[numUsed + 1];
//    for(int i = 0; i < index; i++ )
//    {
//        newArr[i] = arr[i];
//    }
//    newArr[index] = elem;
//    for(int i = index; i < numAllocated; i++ )
//    {
//        newArr[i + 1] = arr[i];
//    }
//    delete[] arr; /* everytime the old new arr[] gets deleted, so the at any point there is one arr
//                    in heap, which is deleted in deconstructor.
//                    also, delete[] knows that it is deleting a what arr is pointing to in heap,
//                    arr itself is on stack.
//                    "delete" however deletes what it takes, which itself is sitting on heap, not what
//                    it points to.
//                    */
//    numAllocated *= 2;
//    arr = newArr;
//    numUsed++;
//    numAllocated++;


//}
//#include "myVector.cpp"

#endif
