////#include "myVector.h"
////#include <string>
////#include <stdexcept>

//template <typename elemType>
//myVector<elemType>::myVector()
//{
//    arr = new elemType[2];
//    numAllocated = 2;
//    numUsed = 0;
//}

//template <typename elemType>
//myVector<elemType>::~myVector(){
//    delete[] arr;
//}

//template <typename elemType>
//int myVector<elemType>::size()
//{
//    return numUsed;
//}

//template <typename elemType>
//void myVector<elemType>::add(elemType s)
//{
//    if (numUsed == numAllocated)
//        doubleCapacity();
//    arr[numUsed++] = s;
//}

//template <typename elemType>
//elemType myVector<elemType>::getAt(int index)
//{
//    if (index < 0 || index > size())
//        std::cout << "out of bounds";
//    return arr[index];
//}

//template <typename elemType>
//void myVector<elemType>::doubleCapacity()
//{
//    std::cout << "doubling capacity" << std::endl;
//    std::string *newArr = new elemType[2 * numAllocated];
//    for(int i = 0; i < numAllocated ; i++ )
//    {
//        newArr[i] = arr[i];
//    }
//    delete[] arr; /* everytime the old new arr[] gets deleted, so the at any point there is one arr
//                    in heap, which is deleted in deconstructor.
//                    also, delete[] knows that it is deleting a what arr is pointing to in heap,
//                    arr itself is on stack.
//                    "delete" however deletes what it takes, which itself is sitting on heap, not what
//                    it points to.
//                    */
//    numAllocated *= 2;
//    arr = newArr;

//}
