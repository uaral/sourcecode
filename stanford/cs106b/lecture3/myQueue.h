#ifndef _MyQueue_H
#define _MyQueue_H

template <typename elemType>
class myQueue
{
public:
    myQueue();
    ~myQueue();
    bool isEmpty();
    void enqueue(elemType);
    elemType dequeue();

private:

    struct cellT {
        elemType val;
        cellT * next;
    };

    cellT* head; //aka, front
    cellT* tail;

};

template <typename elemType>
myQueue<elemType>::~myQueue()
{
    //delete all cells
}


template <typename elemType>
myQueue<elemType>::myQueue()
{
    head = NULL;
    tail = NULL;
}

template <typename elemType>
bool myQueue<elemType>::isEmpty()
{
    if(head != NULL)
        return false;
     else
        return true;
}

template <typename elemType>
void myQueue<elemType>::enqueue(elemType e)
{
    cellT* newCell = new cellT;
    newCell->val = e;
    newCell->next = NULL;
    if(isEmpty())
        head = tail = newCell;
    else
    {
        tail->next = newCell;
        newCell->next = tail;
        tail = newCell;
    }
}

template <typename elemType>
elemType myQueue<elemType>::dequeue()
{
    if (isEmpty())
        return NULL;
    cellT* old = head;
    head = old->next;
    return old->val;
}

#endif
