#ifndef _MyStack_H
#define _MyStack_H
#include "myVector.h"

template <typename elemType>
class myStack
{
public:
    myStack();
    ~myStack();
    void push(elemType);
    elemType pop();

private:
    myVector<elemType> s;

};

template <typename elemType>
myStack<elemType>::~myStack()
{
}

template <typename elemType>
myStack<elemType>::myStack()
{
}

template <typename elemType>
void myStack<elemType>::push(elemType e)
{
   s.add(e);
}

template <typename elemType>
elemType myStack<elemType>::pop()
{
    if (s.size() == 0)
        return NULL;
    elemType tmp = s.getAt(s.size() - 1);
    s.deleteLast();
    return tmp;
}

#endif
