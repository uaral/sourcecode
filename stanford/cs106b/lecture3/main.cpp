#include <iostream>
#include <fstream>
#include <string>
#include "myVector.h"
#include "myStack.h"
#include "myQueue.h"


using namespace std;

struct statistics {
    int min;
    float avg;
    int max;
};

void makeStatistcs(std::string filename)
{
    statistics summaryReport {0,0.0,0};
    std::ifstream fileHandler;
    fileHandler.open(filename.c_str());
    std::string line;
    float counter = 0.0;
    while(getline(fileHandler, line))
    {
        int score = std::stoi(line);
        score < summaryReport.min ? summaryReport.min : score;
        score > summaryReport.max ? summaryReport.max : score;
        summaryReport.avg = ((summaryReport.avg * (counter) ) + score ) / ++counter;
        cout << " line is " << line << endl;
        cout << summaryReport.avg << endl;
    }

}

void censorString(string &str, string &chs)
{
    for(int i = 0; i < str.length(); i++)
    {
        for(int j = 0; j < chs.length(); j++)
        {
            std::cout << str[i] << " VS " << chs[j] << endl;
            if (str[i] == chs[j])
            {
                str = str.erase(i , 1);
                break;
            }
        }
    }
    std::cout << str << endl;
}

void useQueue()
{
    myQueue<int> q;
    q.enqueue(1);
    q.enqueue(2);
    q.enqueue(4);
    q.enqueue(3);
        std::cout << q.dequeue() << std::endl;
        std::cout << q.dequeue() << std::endl;
        std::cout << q.dequeue() << std::endl;
        std::cout << q.dequeue() << std::endl;
//    while(!q.isEmpty())
//        std::cout << q.dequeue() << std::endl;

}

void useVector()
{
   std::cout << "using vector\n";
//   myVector<std::string> v;
   myVector<int> v;
   v.add(1);
   v.add(2);
   v.add(4);
   v.add(5);
   for (int i = 0; i < v.size() ; i++)
   {
       std::cout << v.getAt(i) << std::endl;
   }
   v.insertAt(2, 3);
   for (int i = 0; i < v.size() ; i++)
   {
       std::cout << v.getAt(i) << std::endl;
   }
   std::cout << v.getAt(7) << std::endl;
//   myVector<int> w;
//   w = v; /* shallow copy, will cause problems in for example,
//           * deconstructor because it will cause double delete
//           * solution is to provide deep copy copy constructor, or disallow copying
//           */





}

void useStack()
{
   myStack<string> s;
   s.push("a1");
   s.push("a5");
   s.push("a3");
   std::cout << s.pop() << std::endl;
   std::cout << s.pop() << std::endl;
   std::cout << s.pop() << std::endl;
//   std::cout << s.pop() << std::endl;
}

int main()
{
    useQueue();
//    useVector();
//      useStack();
//    makeStatistcs("scores");//cwd is build dir, so, file must be placed there
//    std::string str ("stanfordUniversity");
//    std::string chs ("pegi");
//    std::cout << str << endl;
//    censorString(str, chs);
//    cout << "2World!" << endl;
//    std::cout << str << endl;
    return 0;
}

class scanner {
    public:
    scanner();
    ~scanner();
};


