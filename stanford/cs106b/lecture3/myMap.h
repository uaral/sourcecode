#ifndef _MyMap_H
#define _MyMap_H
#include "myVector.h"

template <typename elemType>
class myMap
{
public:
    myMap();
    ~myMap();

    bool isEmpty();
    void add(elemType, std::string);
    elemType get();
    bool hasNext();

private:

    struct pair {
        elemType val;
        std::string key;
    };

    myVector<pair> v;

};

template <typename elemType>
myMap<elemType>::~myMap()
{
    //delete entire map
}


template <typename elemType>
myMap<elemType>::myMap()
{
}

template <typename elemType>
bool myMap<elemType>::isEmpty()
{
    if(v.size() == 0)
        return true;
     else
        return false;
}
template <typename elemType>
void myMap<elemType>::add(elemType e, std::string key)
{
    int i;
    for(i = 0; i < v.size(); i++)
    {
        if ((v[i].key == key))
            v[i].val = e;

        else {
            pair p = new pair(e, key);
            v.add(p);
        }
}


#endif
